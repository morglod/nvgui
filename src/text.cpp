#include "nvgui/text.hpp"

#include <tinyxml2/tinyxml2.h>

namespace nvgui {


void Text::setText(std::string const& text) {
    _text = text;
    onTextChanged(text);
}
void Text::setFont(std::string const& font) {
    _font = font;
    onFontChanged(font);
}

void Text::setBlur(float b) {
    _blur = b;
    onBlurChanged(b);
}

Text::Text() : Rect() {
}

Text::Text(std::string const& name, std::string const& text, std::string const& font) : _text(text), _font(font) {
    setName(name);
}

Text::~Text() {
}

void Text::onTextChanged(std::string const& text) {
}

void Text::onFontChanged(std::string const& font) {
}

void Text::onBlurChanged(float b) {
}

void Text::onFrame(Context* ctx) {
    ctx->push();
    ctx->fontBlur(_blur);
    ctx->fontSize(getSize().y);
    ctx->fontFace(_font.c_str());
    ctx->fillColor(getFgColor());
    const auto pos = getPos();
    ctx->text(pos.x, pos.y, _text.c_str(), 0);
    ctx->pop();
}


tinyxml2::XMLNode* Text::saveXml(tinyxml2::XMLDocument* doc, tinyxml2::XMLNode* root) {
    auto element = doc->NewElement("Text");
    writeXml(element);
    auto node = static_cast<tinyxml2::XMLNode*>(element);
    root->InsertFirstChild(node);
    return node;
}

void Text::writeXml(tinyxml2::XMLElement* element) {
    Rect::writeXml(element);

    if(_text.size() != 0) element->SetAttribute("text", _text.c_str());
    if(_font.size() != 0) element->SetAttribute("font", _font.c_str());
    element->SetAttribute("blur", _blur);
}

void Text::loadXml(tinyxml2::XMLElement* element) {
    Rect::loadXml(element);

    auto text_str = element->Attribute("text");
    auto font_str = element->Attribute("font");

    if(text_str != 0) _text = std::string(text_str);
    if(font_str != 0) _font = std::string(font_str);

    float b;
    element->QueryFloatAttribute("blur", &b);
    setBlur(b);
}

}
