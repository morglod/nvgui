#include "nvgui/nvgui.hpp"
#include "nvgui/rect.hpp"
#include "nvgui/text.hpp"
#include "nvgui/input.hpp"
#include "nvgui/button.hpp"
#include "nvgui/xml.hpp"

#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

using namespace std;

std::function<void (glm::vec2 const&)> onMouseMove = nullptr; // (mousePos)
std::function<void (int, bool, nvgui::Input::Mod const&)> onMouseButton = nullptr; // (button, down, mods)

void glfwMouseMove(GLFWwindow* window, double x, double y) {
    onMouseMove({(float)x, (float)y});
}

void glfwMouseButton(GLFWwindow* window, int button, int action, int mods) {
    onMouseButton(button, action == GLFW_PRESS, (nvgui::Input::Mod)mods);
}

nvgui::NodePtr createTestUI(const int width, const int height) {
    auto rootNode = std::make_shared<nvgui::Node>();
	rootNode->setName("root");

	auto box1 = rootNode->create<nvgui::Rect>("box1", glm::vec2{100,100},glm::vec2{100,300});
	auto box2 = rootNode->create<nvgui::Rect>("box2", glm::vec2{300,100},glm::vec2{100,100});

	box1->setBgColor(nvgui::Color(255,0,0,255));
	box2->setBgColor(nvgui::Color(0,255,0,255));

	auto box3 = box1->create<nvgui::Rect>("box3", glm::vec2{10,10},glm::vec2{50,50});
	box3->setBgColor(nvgui::Color(0,0,255,255));

	auto text1 = rootNode->create<nvgui::Text>("text1", "Hello world", "default");
	text1->setPos(glm::vec2{70,200});
	text1->setSize(glm::vec2(0,18));
	text1->setFgColor(nvgui::Color(0,0,255,255));

	auto button = rootNode->create<nvgui::Button>("button1", "Test", "default", 20, glm::vec2{400, 400}, glm::vec2{100,20});

    auto exitButton = rootNode->create<nvgui::Button>("exitButton", "X", "default", 20, glm::vec2{width-15, 0}, glm::vec2{18,15});
    exitButton->setFgColor(nvgui::Color(189, 0, 0, 255));
    exitButton->setBgColor(nvgui::Color(255, 97, 97, 255));

    return rootNode;
}

int main()
{
    const int width = 800, height = 600;

	if (!glfwInit()) {
		printf("Failed to init GLFW.");
		return -1;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_STENCIL_BITS, 8);

    GLFWwindow* window = glfwCreateWindow(width, height, "nvgui test", NULL, NULL);
	if (!window) {
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	glfwSetCursorPosCallback(window, glfwMouseMove);
	glfwSetMouseButtonCallback(window, glfwMouseButton);

	glewExperimental = GL_TRUE;
    if(glewInit() != GLEW_OK) {
		printf("Could not init glew.\n");
		glfwTerminate();
		return -1;
	}

    bool exitFromApp = false;

	nvgui::Context vg;
	if(!vg.init({width, height}, 1.0f, true)) {
		printf("Could not init nanovg.\n");
		glfwTerminate();
		return -1;
	}

	vg.createFont("Roboto-Regular.ttf", "default");

	nvgui::NodePtr rootNode;
	onMouseMove = [&rootNode](glm::vec2 const& mousePos) {
	    rootNode->mouseMove(mousePos);
	};

	onMouseButton = [&rootNode](int button, bool down, nvgui::Input::Mod const& mods) {
	    rootNode->mouseButton(button, down, mods);
	};

	//rootNode = createTestUI(width, height);
	//xml.save("test.xml", rootNode);

    nvgui::Xml xml;
    xml.init();
    rootNode = xml.load("test.xml");

    //Setup callbacks
    rootNode->findT<nvgui::Button>("button1")->setCallback(
    [](nvgui::Button* _this, int buttonId, bool down, nvgui::Input::Mod const& mods) {
        auto label = _this->getLabel();
        if(down) {
            label->setText("Pressed by "+std::to_string(buttonId));
            _this->setBgColor(nvgui::Color(17, 250, 0, 255));
            _this->setFgColor(nvgui::Color(182, 179, 255, 255));
        }
        else {
            label->setText("Released by "+std::to_string(buttonId));
            _this->setBgColor(nvgui::Color(75, 133, 71, 255));
            _this->setFgColor(nvgui::Color(185, 179, 255, 255));
        }
    });

    rootNode->findT<nvgui::Button>("exitButton")->setCallback(
    [&exitFromApp](nvgui::Button* _this, int buttonId, bool down, nvgui::Input::Mod const& mods) {
        if(down) exitFromApp = true;
    });

	while(!exitFromApp && !glfwWindowShouldClose(window)) {
        glViewport(0, 0, width, height);
        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);

		vg.beginFrame();
        rootNode->frame(&vg);
        vg.endFrame();

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

    vg.shutdown();
	glfwTerminate();

	return 0;
}
