#include "nvgui/xml.hpp"
#include "nvgui/node.hpp"
#include "nvgui/rect.hpp"
#include "nvgui/text.hpp"
#include "nvgui/button.hpp"

#include <tinyxml2/tinyxml2.h>

#include <iostream>

namespace nvgui {

void Xml::init() {
    _factory.registerT<Node>("Node");
    _factory.registerT<Rect>("Rect");
    _factory.registerT<Text>("Text");
    _factory.registerT<Button>("Button");
}

bool Xml::save(std::string const& file, NodePtr const& node) {
    using namespace tinyxml2;
    XMLDocument doc;
    auto root = doc.NewElement("nvgui");
    _saveNode(&doc, root, node);
    doc.InsertFirstChild(root);
    return (doc.SaveFile(file.c_str()) != 0);
}

NodePtr Xml::load(std::string const& file) {
    using namespace tinyxml2;
    XMLDocument doc;
    if(doc.LoadFile(file.c_str()) != 0) return nullptr;
    XMLElement* xmlRoot = doc.FirstChildElement("nvgui");

    auto rootElement = xmlRoot->FirstChildElement(); //load top (saved) element
    return _loadNode(&_factory, rootElement, nullptr);
}

void Xml::_saveNode(tinyxml2::XMLDocument* doc, tinyxml2::XMLNode* root, nvgui::NodePtr const& node) {
    if(node == nullptr) return;
    auto parent = node->saveXml(doc, root);
    if(parent == 0) return;
    for(nvgui::NodePtr const& child : node->_children) _saveNode(doc, parent, child);
}

NodePtr Xml::_loadNode(NodeFactory* factory, tinyxml2::XMLElement* thisXmlNode, nvgui::NodePtr const& parentNode) {
    if(thisXmlNode == 0) return nullptr;

    auto thisNode = factory->create(thisXmlNode->Name());

    _loadNode(factory, thisXmlNode->FirstChildElement(), thisNode);

    if(parentNode != nullptr) {
        parentNode->attach(thisNode, true);
        auto nextXmlNode = thisXmlNode->NextSibling();
        if(nextXmlNode != 0) _loadNode(factory, nextXmlNode->ToElement(), parentNode);
    }

    // deffered init (to be sure, all children attached)
    thisNode->loadXml(thisXmlNode);

    return thisNode;
}

}
