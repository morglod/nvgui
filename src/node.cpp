#include "nvgui/node.hpp"
#include "nvgui/nvgui.hpp"

#include <algorithm>
#include <tinyxml2/tinyxml2.h>

namespace nvgui {

void Node::attach(NodePtr const& child, bool insertFirst) {
    if(insertFirst) _children.insert(_children.begin(), child);
    else _children.push_back(child);
    onAttached(child);
}

void Node::detach(NodePtr const& child) {
    auto it = std::find(_children.begin(), _children.end(), child);
    if(it == _children.end()) return;
    _children.erase(it);
    onDetached(child);
}

void Node::frame(Context* ctx) {
    ctx->push();
    onFrame(ctx);
    for(size_t i = 0; i < _children.size(); ++i) _children[i]->frame(ctx);
    onFrameEnd(ctx);
    ctx->pop();
}

void Node::setVisible(bool state, bool children) {
    _visible = state;
    if(children) {
        for(auto const& child : _children) child->setVisible(state, children);
    }
    onVisibilityChanged(state, children);
}

void Node::setName(std::string const& name) {
    _name = name;
    onNameChanged(name);
}

void Node::mouseMove(glm::vec2 const& mousePos) {
    for(auto const& child : _children) child->mouseMove(mousePos);
    onMouseMove(mousePos);
}

void Node::mouseButton(int button, bool down, Input::Mod const& mods) {
    for(auto const& child : _children) child->mouseButton(button, down, mods);
    onMouseButton(button, down, mods);
}

NodePtr Node::find(std::string const& name, int maxLevel) {
    for(auto const& child : _children)
        if(child->getName() == name) return child;
    if(maxLevel == -1 || maxLevel != 0)
        for(auto const& child : _children)
            return child->find(name, maxLevel-1);
    return nullptr;
}

void Node::onAttached(NodePtr const& child) {
}

void Node::onDetached(NodePtr const& child) {
}

void Node::onFrame(Context* ctx) {
}

void Node::onFrameEnd(Context* ctx) {
}

void Node::onVisibilityChanged(bool state, bool children) {
}

void Node::onMouseMove(glm::vec2 const& mousePos) {
}

void Node::onMouseButton(int button, bool down, Input::Mod const& mods) {
}

void Node::onNameChanged(std::string const& name) {
}

tinyxml2::XMLNode* Node::saveXml(tinyxml2::XMLDocument* doc, tinyxml2::XMLNode* root) {
    auto element = doc->NewElement("Node");
    writeXml(element);
    auto node = static_cast<tinyxml2::XMLNode*>(element);
    root->InsertFirstChild(node);
    return node;
}

void Node::writeXml(tinyxml2::XMLElement* element) {
    if(_name.size() != 0) element->SetAttribute("name", _name.c_str());
}

void Node::loadXml(tinyxml2::XMLElement* element) {
    auto str = element->Attribute("name");
    if(str != 0) _name = std::string(str);
}

Node::Node() {
}

Node::Node(std::string const& name, NodePtr const& parent) : _parent(parent) {
    setName(name);
}

Node::~Node() {
}

}
