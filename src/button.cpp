#include "nvgui/button.hpp"
#include "nvgui/text.hpp"

#include <tinyxml2/tinyxml2.h>

namespace nvgui {

void Button::resetLabel(std::string const& text, std::string const& font, float size) {
    if(_label != nullptr) {
        detachT(_label);
        _label = nullptr;
    }

    _label = create<nvgui::Text>(getName()+"-label", text, font);
    _label->setPos(getPos() + glm::vec2(0,size-4));
    _label->setSize({0.0f, size});
	_label->setFgColor(getFgColor());

    onLabelReset(_label, text, font, size);
}

void Button::setCallback(Callback const& cb) {
    _callback = cb;
    onCallbackChanged(cb);
}

void Button::onCallbackChanged(Callback const& cb) {
}

void Button::onMouseButton(int button, bool down, Input::Mod const& mods) {
    if(!isMouseOver() || _callback == nullptr) return;
    _callback(this, button, down, mods);
}

void Button::onLabelReset(nvgui::TextPtr const& textElement, std::string const& text, std::string const& font, float size) {
}

void Button::onFgColorChanged(nvgui::Color const& color) {
    Rect::onFgColorChanged(color);
    if(_label != nullptr) _label->setFgColor(color);
}

tinyxml2::XMLNode* Button::saveXml(tinyxml2::XMLDocument* doc, tinyxml2::XMLNode* root) {
    auto element = doc->NewElement("Button");
    writeXml(element);
    auto node = static_cast<tinyxml2::XMLNode*>(element);
    root->InsertFirstChild(node);
    return node;
}

void Button::writeXml(tinyxml2::XMLElement* element) {
    Rect::writeXml(element);
    if(_label == nullptr) element->SetAttribute("label_name", "");
    else element->SetAttribute("label_name", _label->getName().c_str());
}

void Button::loadXml(tinyxml2::XMLElement* element) {
    Rect::loadXml(element);

    auto labelName = element->Attribute("label_name");
    if(labelName != 0) {
        _label = findT<nvgui::Text>(std::string(labelName));
    }
}

Button::Button() : Rect() {
}

Button::Button(std::string const& name, std::string const& text, std::string const& font, float textSize, glm::vec2 const& pos, glm::vec2 const& size, Callback const& cb) : Rect(name, pos, size) {
    setName(name);
    setCallback(cb);
    resetLabel(text, font, textSize);
}

Button::~Button() {
}

}
