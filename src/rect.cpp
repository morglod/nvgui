#include "nvgui/rect.hpp"
#include "nvgui/nvgui.hpp"

#include <tinyxml2/tinyxml2.h>

#include <iostream>

namespace nvgui {

void Rect::setFgColor(nvgui::Color const& color) {
    _fgColor = color;
    onFgColorChanged(color);
}

void Rect::setBgColor(nvgui::Color const& color) {
    _bgColor = color;
    onBgColorChanged(color);
}

void Rect::setPos(glm::vec2 const& pos) {
    _pos = pos;
    onPosChanged(pos);
}

void Rect::setSize(glm::vec2 const& size) {
    _size = size;
    onSizeChagned(size);
}

void Rect::setCornerRadius(float r) {
    _cornerRadius = r;
    onCornerRadiusChanged(r);
}

Rect::Rect() : Node() {
}

Rect::Rect(std::string const& name, glm::vec2 const& pos, glm::vec2 size) : Node(), _pos(pos), _size(size) {
    setName(name);
}

Rect::~Rect() {
}

void Rect::onFgColorChanged(nvgui::Color const& color) {
}

void Rect::onBgColorChanged(nvgui::Color const& color) {
}

void Rect::onPosChanged(glm::vec2 const& pos) {
}

void Rect::onSizeChagned(glm::vec2 const& size) {
}

void Rect::onFrame(Context* ctx) {
    if(isVisible()) {
        ctx->beginPath();
        ctx->fillColor(_bgColor);
        if(_cornerRadius == 0.0f) ctx->rect(_pos.x, _pos.y, _size.x, _size.y);
        else ctx->roundedRect(_pos.x, _pos.y, _size.x, _size.y, _cornerRadius);
        ctx->fill();
    }
}

void Rect::onCornerRadiusChanged(float r) {
}

void Rect::onMouseMove(glm::vec2 const& mousePos) {
    const auto c = (_pos+_size);
    _mouseOver = (mousePos.x > _pos.x && mousePos.y > _pos.y &&
                  mousePos.x < c.x && mousePos.y < c.y);
}


tinyxml2::XMLNode* Rect::saveXml(tinyxml2::XMLDocument* doc, tinyxml2::XMLNode* root) {
    auto element = doc->NewElement("Rect");
    writeXml(element);
    auto node = static_cast<tinyxml2::XMLNode*>(element);
    root->InsertFirstChild(node);
    return node;
}

void Rect::writeXml(tinyxml2::XMLElement* element) {
    Node::writeXml(element);

    element->SetAttribute("pos_x", (float)_pos.x);
    element->SetAttribute("pos_y", (float)_pos.y);

    element->SetAttribute("size_x", (float)_size.x);
    element->SetAttribute("size_y", (float)_size.y);

    element->SetAttribute("fg_color_r", (unsigned int)_fgColor.x);
    element->SetAttribute("fg_color_g", (unsigned int)_fgColor.y);
    element->SetAttribute("fg_color_b", (unsigned int)_fgColor.z);
    element->SetAttribute("fg_color_a", (unsigned int)_fgColor.w);

    element->SetAttribute("bg_color_r", (unsigned int)_bgColor.x);
    element->SetAttribute("bg_color_g", (unsigned int)_bgColor.y);
    element->SetAttribute("bg_color_b", (unsigned int)_bgColor.z);
    element->SetAttribute("bg_color_a", (unsigned int)_bgColor.w);

    element->SetAttribute("cornerRadius", (float)_cornerRadius);
}

void Rect::loadXml(tinyxml2::XMLElement* element) {
    Node::loadXml(element);

    float x, y;

    element->QueryFloatAttribute("pos_x", &x);
    element->QueryFloatAttribute("pos_y", &y);

    setPos({x,y});

    element->QueryFloatAttribute("size_x", &x);
    element->QueryFloatAttribute("size_y", &y);

    setSize({x,y});

    unsigned int r, g, b, a;

    element->QueryUnsignedAttribute("fg_color_r", &r);
    element->QueryUnsignedAttribute("fg_color_g", &g);
    element->QueryUnsignedAttribute("fg_color_b", &b);
    element->QueryUnsignedAttribute("fg_color_a", &a);
    setFgColor(nvgui::Color((unsigned char)r, (unsigned char)g, (unsigned char)b, (unsigned char)a));

    element->QueryUnsignedAttribute("bg_color_r", &r);
    element->QueryUnsignedAttribute("bg_color_g", &g);
    element->QueryUnsignedAttribute("bg_color_b", &b);
    element->QueryUnsignedAttribute("bg_color_a", &a);
    setBgColor(nvgui::Color((unsigned char)r, (unsigned char)g, (unsigned char)b, (unsigned char)a));

    float cR;
    element->QueryFloatAttribute("cornerRadius", &cR);
    setCornerRadius(cR);
}

}
