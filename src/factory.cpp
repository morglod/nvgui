#include "nvgui/factory.hpp"

namespace nvgui {

NodePtr NodeFactory::create(std::string const& name) {
    if(_types.find(name) == _types.end()) return nullptr;
    return _types[name]();
}

void NodeFactory::unregister(std::string const& name) {
    auto it = _types.find(name);
    if(it == _types.end()) return;
    _types.erase(it);
}

}
