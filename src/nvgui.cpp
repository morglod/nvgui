#include "nvgui/nvgui.hpp"

#include <stdio.h>

#define NANOVG_GL3_IMPLEMENTATION
#include <nanovg_gl.h>

#include <algorithm>

namespace nvgui {

bool Context::init(glm::ivec2 const& size, float pxRatio, bool antialiasing) {
    int flags = NVG_STENCIL_STROKES | ((antialiasing) ? NVG_ANTIALIAS : 0);
    _vg = nvgCreateGL3(flags);
    _size = size;
    _pxRatio = pxRatio;
	return (_vg != 0);
}

void Context::shutdown() {
    if(_vg == 0) return;
	nvgDeleteGL3(_vg);
	_vg = 0;
}

void Context::beginFrame() {
    nvgBeginFrame(_vg, _size.x, _size.y, _pxRatio);
}

void Context::cancelFrame() {
    nvgCancelFrame(_vg);
}

void Context::endFrame() {
    nvgEndFrame(_vg);
}

void Context::push() {
    nvgSave(_vg);
}

void Context::pop() {
    nvgRestore(_vg);
}

void Context::reset() {
    nvgReset(_vg);
}

void Context::strokeColor(Color const& rgba) {
    nvgStrokeColor(_vg, colorNvg(rgba));
}

void Context::strokePaint(NVGpaint const& paint) {
    nvgStrokePaint(_vg, paint);
}

void Context::fillColor(Color const& rgba) {
    nvgFillColor(_vg, colorNvg(rgba));
}

void Context::fillPaint(NVGpaint const& paint) {
    nvgFillPaint(_vg, paint);
}

void Context::miterLimit(float const& limit) {
    nvgMiterLimit(_vg, limit);
}

void Context::strokeWith(float const& size) {
    nvgStrokeWidth(_vg, size);
}

void Context::lineCap(int cap) {
    nvgLineCap(_vg, cap);
}

void Context::lineJoin(int join) {
    nvgLineJoin(_vg, join);
}

void Context::globalAlpha(float alpha) {
    nvgGlobalAlpha(_vg, alpha);
}

void Context::resetTransform() {
    nvgResetTransform(_vg);
}

void Context::transform(float const& a, float const& b, float const& c, float const& d, float const& e, float const& f) {
    nvgTransform(_vg, a, b, c, d, e, f);
}

void Context::translate(float const& x, float const& y) {
    nvgTranslate(_vg, x, y);
}

void Context::rotate(float const& angle) {
    nvgRotate(_vg, angle);
}

void Context::scale(float const& x, float const& y) {
    nvgScale(_vg, x, y);
}

int Context::createImage(std::string const& filename, NVGimageFlags flags) {
    int i = 0;
    if( (i = nvgCreateImage(_vg, filename.c_str(), flags)) != -1 ) _imgs[filename] = i;
    return i;
}

int Context::createImageMem(std::string const& cacheFilename, unsigned char* data, int const& ndata, NVGimageFlags flags) {
    int i = 0;
    if( (i = nvgCreateImageMem(_vg, flags, data, ndata)) != -1 ) _imgs[cacheFilename] = i;
    return i;
}

int Context::createImageRGBA(std::string const& cacheFilename, int const& w, int const& h, const unsigned char* data, NVGimageFlags flags) {
    int i = 0;
    if( (i = nvgCreateImageRGBA(_vg, w, h, flags, data)) != -1 ) _imgs[cacheFilename] = i;
    return i;
}

void Context::updateImage(int const& image, const unsigned char* data) {
    nvgUpdateImage(_vg, image, data);
}

glm::ivec2 Context::imageSize(int const& image) {
    glm::ivec2 size;
    nvgImageSize(_vg, image, &size.x, &size.y);
    return size;
}

void Context::deleteImage(int const& id) {
    if(id == -1) return;
    nvgDeleteImage(_vg, id);
    auto it = std::find_if(_imgs.begin(), _imgs.end(), [id](std::pair<std::string, int> const& pair) -> bool { return (pair.second == id); });
    if(it == _imgs.end()) return;
    _imgs.erase(it);
}

int Context::findImage(std::string const& filename) {
    auto it = _imgs.find(filename);
    if(it == _imgs.end()) return -1;
    return it->second;
}

NVGpaint Context::createPaint(LinearGradient const& linearGradient) {
    return nvgLinearGradient(_vg,   linearGradient.startPos.x, linearGradient.startPos.y,
                                    linearGradient.endPos.x, linearGradient.endPos.y,
                                    colorNvg(linearGradient.startColor), colorNvg(linearGradient.endColor));
}

NVGpaint Context::createPaint(BoxGradient const& boxGradient) {
    return nvgBoxGradient(_vg,  boxGradient.pos.x, boxGradient.pos.y,
                                boxGradient.size.x, boxGradient.size.y,
                                boxGradient.cornerRadius, boxGradient.blurFeather,
                                colorNvg(boxGradient.innerColor), colorNvg(boxGradient.outerColor));
}

NVGpaint Context::createPaint(RadialGradient const& radialGradient) {
    return nvgRadialGradient(_vg,   radialGradient.centerPos.x, radialGradient.centerPos.y,
                                    radialGradient.innerRadius, radialGradient.outerRadius,
                                    colorNvg(radialGradient.startColor), colorNvg(radialGradient.endColor));
}

NVGpaint Context::createPaint(ImagePattern const& imagePattern) {
    return nvgImagePattern(_vg, imagePattern.leftTopPos.x, imagePattern.leftTopPos.y,
                                imagePattern.size.x, imagePattern.size.y,
                                imagePattern.angle, imagePattern.image, imagePattern.repeat);
}

void Context::scissor(glm::vec2 const& pos, glm::vec2 const& size) {
    nvgScissor(_vg, pos.x, pos.y, size.x, size.y);
}

void Context::resetScissor() {
    nvgResetScissor(_vg);
}

void Context::beginPath() {
    nvgBeginPath(_vg);
}

void Context::moveTo(float x, float y) {
    nvgMoveTo(_vg, x, y);
}

void Context::lineTo(float x, float y) {
    nvgLineTo(_vg, x, y);
}

void Context::bezierTo(float c1x, float c1y, float c2x, float c2y, float x, float y) {
    nvgBezierTo(_vg, c1x, c1y, c2x, c2y, x, y);
}

void Context::arcTo(float x1, float y1, float x2, float y2, float radius) {
    nvgArcTo(_vg, x1, y1, x2, y2, radius);
}

void Context::closePath() {
    nvgClosePath(_vg);
}

void Context::pathWinding(int dir) {
    nvgPathWinding(_vg, dir);
}

void Context::arc(float cx, float cy, float r, float a0, float a1, int dir) {
    nvgArc(_vg, cx, cy, r, a0, a1, dir);
}

void Context::rect(float x, float y, float w, float h) {
    nvgRect(_vg, x, y, w, h);
}

void Context::roundedRect(float x, float y, float w, float h, float r) {
    nvgRoundedRect(_vg, x, y, w, h, r);
}

void Context::ellipse(float cx, float cy, float rx, float ry) {
    nvgEllipse(_vg, cx, cy, rx, ry);
}

void Context::circle(float cx, float cy, float r) {
    nvgCircle(_vg, cx, cy, r);
}

void Context::fill() {
    nvgFill(_vg);
}

void Context::stroke() {
    nvgStroke(_vg);
}

int Context::createFont(const char* filename, const char* fontname) {
    return nvgCreateFont(_vg, fontname, filename);
}

int Context::createFontMem(const char* fontname, unsigned char* data, int ndata, int freeData) {
    return nvgCreateFontMem(_vg, fontname, data, ndata, freeData);
}

int Context::findFont(const char* fontname) {
    return nvgFindFont(_vg, fontname);
}

void Context::fontSize(float size) {
    nvgFontSize(_vg, size);
}

void Context::textLetterSpacing(float spacing) {
    nvgTextLetterSpacing(_vg, spacing);
}

void Context::fontBlur(float blur) {
    nvgFontBlur(_vg, blur);
}

void Context::textAlign(int align) {
    nvgTextAlign(_vg, align);
}

void Context::fontFaceId(int font) {
    nvgFontFaceId(_vg, font);
}

void Context::fontFace(const char* font) {
    nvgFontFace(_vg, font);
}

float Context::text(float x, float y, const char* string, const char* end) {
    return nvgText(_vg, x, y, string, end);
}

float Context::textBounds(float x, float y, const char* string, const char* end, float* bounds) {
    return nvgTextBounds(_vg, x, y, string, end, bounds);
}
/*
void Context::vertMetrics(float* ascender, float* descender, float* lineh) {
    nvgVertMetrics(_vg, ascender, descender, lineh);
}*/

Context::Context() {
}

Context::~Context() {
    shutdown();
}

}
