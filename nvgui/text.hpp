#pragma once

#include "rect.hpp"

#include <string>

namespace nvgui {

class Text : public Rect {
public:
    void setText(std::string const& text);
    inline std::string getText() const { return _text; }
    void setFont(std::string const& font);
    inline std::string getFont() const { return _font; }
    void setBlur(float b);
    inline float getBlur() const { return _blur; }

    Text();
    Text(std::string const& name, std::string const& text, std::string const& font);
    virtual ~Text();

    tinyxml2::XMLNode* saveXml(tinyxml2::XMLDocument* doc, tinyxml2::XMLNode* root) override;
    void writeXml(tinyxml2::XMLElement* element) override;
    void loadXml(tinyxml2::XMLElement* element) override;

protected:
    virtual void onTextChanged(std::string const& text);
    virtual void onFontChanged(std::string const& font);
    virtual void onBlurChanged(float b);

    void onFrame(Context* ctx) override;

private:
    std::string _text = "";
    std::string _font = "";
    float _blur = 0.0f;
};

typedef std::shared_ptr<Text> TextPtr;

}
