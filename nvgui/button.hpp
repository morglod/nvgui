#pragma once

#include "rect.hpp"

#include <string>
#include <functional>

namespace nvgui {

typedef std::shared_ptr<class Text> TextPtr;

class Button : public Rect {
public:
    typedef std::function<void (Button*, int, bool, Input::Mod const&)> Callback;
    inline Callback getCallback() const { return _callback; }
    void setCallback(Callback const& cb);
    void resetLabel(std::string const& text, std::string const& font, float size);
    inline TextPtr getLabel() { return _label; }

    tinyxml2::XMLNode* saveXml(tinyxml2::XMLDocument* doc, tinyxml2::XMLNode* root) override;
    void writeXml(tinyxml2::XMLElement* element) override;
    void loadXml(tinyxml2::XMLElement* element) override;

    Button();
    Button(std::string const& name, std::string const& text, std::string const& font, float textSize, glm::vec2 const& pos, glm::vec2 const& size, Callback const& cb = nullptr);
    virtual ~Button();

protected:
    virtual void onCallbackChanged(Callback const& cb);
    virtual void onLabelReset(nvgui::TextPtr const& textElement, std::string const& text, std::string const& font, float size);

    void onMouseButton(int button, bool down, Input::Mod const& mods) override;
    void onFgColorChanged(nvgui::Color const& color) override;

private:
    Callback _callback = nullptr; // (const Button*, button, down, mods)
    nvgui::TextPtr _label = nullptr;

};

}
