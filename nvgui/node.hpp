#pragma once

#include "input.hpp"

#include <glm/glm.hpp>

#include <memory>
#include <vector>

namespace tinyxml2 {
    class XMLNode;
    class XMLDocument;
    class XMLElement;
}

namespace nvgui {

typedef std::shared_ptr<class Node> NodePtr;

class Context;

class Node {
    friend class Xml;
public:
    template<class TNode, class... Args>
    inline std::shared_ptr<TNode> create(Args... args) {
        auto tnode = std::shared_ptr<TNode>(new TNode(args...));
        attachT(tnode);
        return tnode;
    }

    template<class T>
    inline void attachT(std::shared_ptr<T> const& child) {
        attach(std::static_pointer_cast<Node>(child));
    }

    template<class T>
    inline void detachT(std::shared_ptr<T> const& child) {
        detach(std::static_pointer_cast<Node>(child));
    }

    template<class T>
    inline std::shared_ptr<T> findT(std::string const& name, int maxLevel = -1) {
        auto n = find(name, maxLevel);
        if(n == nullptr) return nullptr;
        return std::dynamic_pointer_cast<T>(n);
    }

    void attach(NodePtr const& child, bool insertFirst = false);
    void detach(NodePtr const& child);
    NodePtr find(std::string const& name, int maxLevel = -1);

    void frame(Context* ctx);

    inline NodePtr getParent() const { return _parent; }

    void setVisible(bool state, bool children = false);
    inline bool isVisible() const { return _visible; }

    void setName(std::string const& name);
    inline std::string getName() const { return _name; }

    void mouseMove(glm::vec2 const& mousePos);
    void mouseButton(int button, bool down, Input::Mod const& mods);

    virtual tinyxml2::XMLNode* saveXml(tinyxml2::XMLDocument* doc, tinyxml2::XMLNode* root); //return 0 or new node
    virtual void writeXml(tinyxml2::XMLElement* element); //write current type data to this element
    virtual void loadXml(tinyxml2::XMLElement* element);

    Node();
    Node(std::string const& name, NodePtr const& parent);
    virtual ~Node();

protected:
    inline void _setParent(NodePtr const& parent) { _parent = parent; }
    virtual void onAttached(NodePtr const& child);
    virtual void onDetached(NodePtr const& child);
    virtual void onFrame(Context* ctx);
    virtual void onFrameEnd(Context* ctx);
    virtual void onVisibilityChanged(bool state, bool children);
    virtual void onMouseMove(glm::vec2 const& mousePos);
    virtual void onMouseButton(int button, bool down, Input::Mod const& mods);
    virtual void onNameChanged(std::string const& name);

    std::vector<NodePtr> _children;
private:
    std::string _name = "";
    NodePtr _parent = nullptr;
    bool _visible = true;
};

}
