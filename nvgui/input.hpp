#pragma once

namespace nvgui {

class Input final {
public:
    enum Mod : int {
        Mod_Shift = 1,
        Mod_Control = 2,
        Mod_Alt = 4,
        Mod_Super = 8
    };
};

}
