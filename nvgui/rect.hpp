#pragma once

#include "nvgui.hpp"
#include "node.hpp"

#include <glm/glm.hpp>

namespace nvgui {

class Rect : public Node {
public:
    void setFgColor(nvgui::Color const& color);
    void setBgColor(nvgui::Color const& color);
    inline nvgui::Color getFgColor() const { return _fgColor; }
    inline nvgui::Color getBgColor() const { return _bgColor; }

    void setPos(glm::vec2 const& pos);
    void setSize(glm::vec2 const& size);
    inline glm::vec2 getPos() const { return _pos; }
    inline glm::vec2 getSize() const { return _size; }

    void setCornerRadius(float r);
    inline float getCornerRadius() const { return _cornerRadius; }

    inline bool isMouseOver() const { return _mouseOver; }

    tinyxml2::XMLNode* saveXml(tinyxml2::XMLDocument* doc, tinyxml2::XMLNode* root) override;
    void writeXml(tinyxml2::XMLElement* element) override;
    void loadXml(tinyxml2::XMLElement* element) override;

    Rect();
    Rect(std::string const& name, glm::vec2 const& pos, glm::vec2 size);
    virtual ~Rect();

protected:
    virtual void onFgColorChanged(nvgui::Color const& color);
    virtual void onBgColorChanged(nvgui::Color const& color);
    virtual void onPosChanged(glm::vec2 const& pos);
    virtual void onSizeChagned(glm::vec2 const& size);
    virtual void onCornerRadiusChanged(float r);

    void onFrame(Context* ctx) override;
    void onMouseMove(glm::vec2 const& mousePos) override;

private:
    glm::vec2 _pos, _size;
    nvgui::Color    _fgColor = nvgui::Color(255,255,255,255),
                    _bgColor = nvgui::Color(155,155,155,255);
    float _cornerRadius = 0.0f;
    bool _mouseOver = false;
};

}
