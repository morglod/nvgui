#pragma once

#include "factory.hpp"

namespace nvgui {

typedef std::shared_ptr<class Node> NodePtr;

class Xml final {
public:
    void init();

    bool save(std::string const& file, NodePtr const& node);
    NodePtr load(std::string const& file);

    Xml() = default;
private:
    static void _saveNode(tinyxml2::XMLDocument* doc, tinyxml2::XMLNode* root, nvgui::NodePtr const& node);
    static nvgui::NodePtr _loadNode(NodeFactory* factory, tinyxml2::XMLElement* node, nvgui::NodePtr const& root);

    NodeFactory _factory;
};

}
