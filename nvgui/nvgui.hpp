#pragma once

#define NANOVG_GLEW

#ifdef NANOVG_GLEW
    #include <GL/glew.h>
#endif

extern "C" {
#include <nanovg.h>
}

#include <glm/glm.hpp>
#include <string>
#include <unordered_map>

namespace nvgui {

typedef glm::tvec4<unsigned char, glm::highp> Color; // { rgba }
inline NVGcolor colorNvg(Color const& color) { return nvgRGBA(color.x, color.y, color.z, color.w); }

struct LinearGradient {
    glm::vec2 startPos, endPos;
    Color startColor, endColor;
};

struct BoxGradient {
    glm::vec2 pos, size;
    float cornerRadius, blurFeather;
    Color innerColor, outerColor;
};

struct RadialGradient {
    glm::ivec2 centerPos;
    float innerRadius, outerRadius;
    Color startColor, endColor;
};

struct ImagePattern {
    glm::ivec2 leftTopPos, size;
    float angle;
    int image,
        repeat; // flags NVG_REPEATX, NVG_REPEATY
};

/// TODO: Check all new funcs, and implement in Context class.

class Context final {
public:
    bool init(glm::ivec2 const& size, float pxRatio = 1.0f, bool antialiasing = true);
    void shutdown();

    void beginFrame();
    void cancelFrame();
    void endFrame();

    void push();
    void pop();
    void reset();

    void strokeColor(Color const& rgba);
    void strokePaint(NVGpaint const& paint);
    void fillColor(Color const& rgba);
    void fillPaint(NVGpaint const& paint);
    void miterLimit(float const& limit);
    void strokeWith(float const& size);
    void lineCap(int cap); //NVG_ROUND_ NVG_SQUARE
    void lineJoin(int join); //NVG_ROUND, NVG_BEVEL
    void globalAlpha(float alpha);

    void resetTransform();
    void transform(float const& a, float const& b, float const& c, float const& d, float const& e, float const& f);
    void translate(float const& x, float const& y);
    void rotate(float const& angle);
    void scale(float const& x, float const& y);

    int createImage(std::string const& filename, NVGimageFlags flags = NVG_IMAGE_GENERATE_MIPMAPS);
    int createImageMem(std::string const& cacheFilename, unsigned char* data, int const& ndata, NVGimageFlags flags = NVG_IMAGE_GENERATE_MIPMAPS);
    int createImageRGBA(std::string const& cacheFilename, int const& w, int const& h, const unsigned char* data, NVGimageFlags flags = NVG_IMAGE_GENERATE_MIPMAPS);
    void updateImage(int const& image, const unsigned char* data);
    glm::ivec2 imageSize(int const& image);
    void deleteImage(int const& id);
    int findImage(std::string const& filename);

    NVGpaint createPaint(LinearGradient const& linearGradient);
    NVGpaint createPaint(BoxGradient const& boxGradient);
    NVGpaint createPaint(RadialGradient const& radialGradient);
    NVGpaint createPaint(ImagePattern const& imagePattern);

    void scissor(glm::vec2 const& pos, glm::vec2 const& size);
    void resetScissor();

    void beginPath();
    void moveTo(float x, float y);
    void lineTo(float x, float y);
    void bezierTo(float c1x, float c1y, float c2x, float c2y, float x, float y);
    void arcTo(float x1, float y1, float x2, float y2, float radius);
    void closePath();
    void pathWinding(int dir);
    void arc(float cx, float cy, float r, float a0, float a1, int dir);
    void rect(float x, float y, float w, float h);
    void roundedRect(float x, float y, float w, float h, float r);
    void ellipse(float cx, float cy, float rx, float ry);
    void circle(float cx, float cy, float r);
    void fill();
    void stroke();

    int createFont(const char* filename, const char* fontname);
    int createFontMem(const char* fontname, unsigned char* data, int ndata, int freeData);
    int findFont(const char* fontname);
    void fontSize(float size);
    void textLetterSpacing(float spacing);
    void fontBlur(float blur);
    void textAlign(int align);
    void fontFaceId(int font);
    void fontFace(const char* font);
    float text(float x, float y, const char* string, const char* end);
    float textBounds(float x, float y, const char* string, const char* end, float* bounds);
    //void vertMetrics(float* ascender, float* descender, float* lineh);

    inline NVGcontext* getNVG() { return _vg; }

    Context();
    ~Context();
private:
    NVGcontext* _vg = 0;
    glm::ivec2 _size;
    float _pxRatio = 1.0f;
    std::unordered_map<std::string, int> _imgs;
};

}
