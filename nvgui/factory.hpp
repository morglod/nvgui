#pragma once

#include "node.hpp"

#include <string>
#include <unordered_map>
#include <functional>
#include <memory>

namespace nvgui {

typedef std::shared_ptr<class Node> NodePtr;

class NodeFactory final {
public:
    typedef std::function<NodePtr ()> TypeFunc;

    template<class T, typename... ArgsT>
    inline void registerT(std::string const& name, ArgsT... argst) {
        _types[name] = [argst...]() -> NodePtr {
            auto t = new T(argst...);
            auto n = static_cast<nvgui::Node*>(t);
            return std::shared_ptr<nvgui::Node>(n);
        };
    }

    void unregister(std::string const& name);

    NodePtr create(std::string const& name);

    NodeFactory() = default;
    ~NodeFactory() = default;

private:
    std::unordered_map<std::string, TypeFunc> _types;
};

}
